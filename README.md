This is a word for word translation system based on word2vec word embeddings, based on the original experiement by Tomas Milokov and his team at Google.

See the papers in the /papers folder for more information on the experiment and results, or the "how_to_use.txt" for instructions on running the system to translate from Japanese<->English.

You can also use the tools provided to train your own models. The system has been tested on Japanese, English, Spanish, Polish, Czech and a few others with varying results (see paper for further information).

Paper and how_to_use are provided in both Japanese and English. For any questions about the experiement, running the system, how you can further use word2vec to build on machine translation or other comments, feel free to contact me.

This system and the accompanying English and Japanese paper was developed for Prof Kenji Araki at Hokkaido University during a summer internship. Without his valuable input and advice. Tomas Mikolov and Mostafa￼Chatillon provided invaluable insight via personal email about how word2vec works
and how to go out about generating a translation matrix, and the mathematics behind it.