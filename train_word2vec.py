# -*- coding: utf-8 -*-

import gensim
import os
import codecs
import io
import logging
import utils

name = 'en_wiki.bin'

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

class MySentences(object):
    def __init__(self, dirname):
        self.dirname = dirname
    def __iter__(self):
        for fname in os.listdir(self.dirname):
            with open(os.path.join(self.dirname,fname)) as opn:
                for senten in opn:
                    yield senten.split()

directory = 'training_data'

sentences = MySentences(directory)

modelen = gensim.models.Word2Vec(sentences, size=300, window=5, min_count=5, workers=36)

modelen.init_sims(replace=True)

modelen.save(name)