# -*- coding: utf-8 -*-

import gensim
import os
import codecs
import ic
import logging
import pandas as pd 
import numpy as np
import sys


# Log output. Also useful to show program is doing things
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

# models trained using gensim implementation of word2vec
print 'Loading models...'
#model_source = gensim.models.Word2Vec.load('model_jp_wiki')
#model_target = gensim.models.Word2Vec.load('model_en_wiki')

model_target = gensim.models.Word2Vec.load('kyoto_word2vec_models/kyoto-jp-word2vec.bin') #jp
model_source = gensim.models.Word2Vec.load('kyoto_word2vec_models/kyoto-word2vec.bin') #en


# list of word pairs to train translation matrix as csv
# eg:
#  source,target
#  今日は、hello
#  犬、dog
#  猫、cat
print 'Reading training pairs...'
word_pairs = codecs.open('en-jp-8000.csv', 'r', 'utf-8')

pairs = pd.read_csv(word_pairs)

print 'Removing missing vocabulary...'

missing = 0

for n in range (len(pairs)):
	if pairs['english'][n] not in model_source.vocab or pairs['japanese'][n] not in model_target.vocab:
		missing = missing + 1
		pairs = pairs.drop(n)

pairs = pairs.reset_index(drop = True)
print 'Amount of missing vocab: ', missing

# make list of pair words, excluding the missing vocabs 
# removed in previous step
pairs['vector_target'] = [model_target[pairs['japanese'][n]] for n in range (len(pairs))]
pairs['vector_source'] = [model_source[pairs['english'][n]] for n in range (len(pairs))]

# first 5000 from both languages, to train translation matrix
source_training_set = pairs['vector_target'][:5000]
target_training_set = pairs['vector_source'][:5000]

matrix_train_source = pd.DataFrame(target_training_set.tolist()).values
matrix_train_target = pd.DataFrame(source_training_set.tolist()).values

print 'Generating translation matrix'

# Matrix W is given in  http://stackoverflow.com/questions/27980159/fit-a-linear-transformation-in-python
translation_matrix = np.linalg.pinv(matrix_train_source).dot(matrix_train_target).T
print 'Generated translation matrix'

# Returns list of topn closest vectors to vectenter
def most_similar_vector(self, vectenter, topn=5):
    self.init_sims()
    dists = np.dot(self.syn0norm, vectenter)
    if not topn:
        return dists
    best = np.argsort(dists)[::-1][:topn ]
        # ignore (don't return) words from the input
    result = [(self.index2word[sim], float(dists[sim])) for sim in best]
    return result[:topn]

def top_translations(w,numb=5):
    val = most_similar_vector(model_target,translation_matrix.dot(model_source[w]),numb)
    return val

def top_translations_list(w, numb=5):
    val = [top_translations(w,numb)[k][0] for k in range(numb)]
    print "Getting top translations for word : ", w
    return val

#top_matches = [ pairs['target'][n] in top_translations_list(pairs['source'][n]) for n in range(5000,5003)] 

translations_written = 0
# print out source word and translation
def display_translations(translations_written):
    for word_num in range(range_start, range_end):
        translations_written = translations_written + 1
        if (translations_written % 5 == 0):
            print "Transtions written to file: " , translations_written
        
        source_word =  pairs['japanese'][word_num]
        translations = top_translations_list(pairs['japanese'][word_num]) 
        
        with open('translation_results_word2vec.txt','w') as f:
            

            if (pairs['english'][word_num] in translations):
                f.write("---Success, correct translation is : " + pairs['english'][word_num] + "---" + "\n")
            else:
                f.write("---Failed...desired translation was : " + pairs['english'][word_num] + "---" + "\n")
            f.write(source_word + "  " + str(translations) + "\n\n")
        f.close()

def line_prepender(filename, line):
    with open(filename, 'r+') as f:
        content = f.read()
        f.seek(0, 0)
        f.write(line.rstrip('\r\n') + '\n' + content)      

# range to use to check accuracy
range_start = 1500
range_end = 2000



def translate_word(word):
    
    source_word = word
    translations = top_translations_list(source_word) 
    return translations

print "\n\n--- Translate individual words using word2vec train on wikipedia dumps ---\n"
while (1):
    user_input = raw_input("Input word, or \"q\" to quit.\n")

    if user_input == 'q':
        sys.exit()
    else: 
        try:  
            trans = translate_word(user_input)
            print "Translations for ", user_input, " are: ",
            for i in range(0, len(trans)):
                print trans[i], " ",
        except KeyError:
            print "Word is not in wiki model. Cannot translate.\n"



#---------- Accuracy @5 ----------#
accuracy_at_five = [pairs['english'][n] in top_translations_list(pairs['japanese'][n]) for n in range(range_start, range_end)]
print 'Accuracy @5 is ', sum(accuracy_at_five), '/', len(accuracy_at_five)
sum_at_five = sum(accuracy_at_five)
len_at_five = len(accuracy_at_five)
at_five = str(sum_at_five) + " / " + str(len_at_five)

#---------- Accuracy @1 ----------#
accuracy_at_one = [pairs['english'][n] in top_translations_list(pairs['japanese'][n],1) for n in range(range_start, range_end)]
print 'Accuracy @1 is ', sum(accuracy_at_one), '/', len(accuracy_at_one)
sum_at_one = sum(accuracy_at_one)
len_at_one = len(accuracy_at_one)
at_one = str(sum_at_one) + " / " + str(len_at_one)

display_translations(translations_written)
line_prepender('translation_results_tanaka.txt', "Accuracy @5: " + at_five + "\n")
line_prepender('translation_results_tanaka.txt', "Accuracy @1: " + at_one + "\n")










